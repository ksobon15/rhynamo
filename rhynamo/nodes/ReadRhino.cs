﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Rhino.FileIO;
using Rhino.Geometry;

namespace Interoperability.ReadRhino
{
  /// <summary>
  /// Get the Rhino File3dm Object
  /// </summary>
  public static class OpenRhino3dmModel
  {
    /// <summary>
    /// Gets a Rhino file object using a path string.
    /// </summary>
    /// <param name="file">The file or path of the Rhino file (*.3dm)</param>
    /// <returns name="model">The File3dm object (Rhino file)</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    [MultiReturn(new[] { "File3dm", "Name", "Bytes", "Created", "CreatedBy", "LastEdited", "LastEditedBy", "Revision", "Units", "Notes" })]
    public static Dictionary<string, object> Get_RhinoFile(Object rh_file)
    {
      File3dm m_modelfile = null;
      string m_name = null;
      string m_size = null;
      string m_created = null;
      string m_createdby = null;
      string m_edited = null;
      string m_editedby = null;
      string m_revision = null;
      string m_units = null;
      string m_notes = null;

      if (rh_file is System.IO.FileInfo)
      {
        System.IO.FileInfo m_fileinfo = (System.IO.FileInfo)rh_file;

        m_modelfile = File3dm.Read(m_fileinfo.FullName);
        m_name = m_fileinfo.Name;
        m_size = m_fileinfo.Length.ToString();
        m_created = m_fileinfo.CreationTimeUtc.ToString();
        m_createdby = m_modelfile.CreatedBy;
        m_edited = m_fileinfo.LastWriteTimeUtc.ToString();
        m_editedby = m_modelfile.LastEditedBy;
        m_revision = m_modelfile.Revision.ToString();
        m_units = m_modelfile.Settings.ModelUnitSystem.ToString();
        m_notes = m_modelfile.Notes.Notes;
      }
      else if (rh_file is string)
      {
        // get file info
        System.IO.FileInfo m_fileinfo = new System.IO.FileInfo((string)rh_file);

        m_modelfile = File3dm.Read(m_fileinfo.FullName);
        m_name = m_fileinfo.Name;
        m_size = m_fileinfo.Length.ToString();
        m_created = m_fileinfo.CreationTimeUtc.ToString();
        m_createdby = m_modelfile.CreatedBy;
        m_edited = m_fileinfo.LastWriteTimeUtc.ToString();
        m_editedby = m_modelfile.LastEditedBy;
        m_revision = m_modelfile.Revision.ToString();
        m_units = m_modelfile.Settings.ModelUnitSystem.ToString();
        m_notes = m_modelfile.Notes.Notes;
      }

      // return model and info
      return new Dictionary<string, object>
              {
                  { "File3dm", m_modelfile },
                  { "Name", m_name },
                  { "Bytes", m_size },
                  { "Created", m_created },
                  { "CreatedBy", m_createdby },
                  { "LastEdited", m_edited },
                  { "LastEditedBy", m_editedby },
                  { "Revision", m_revision },
                  { "Units", m_units },
                  { "Notes", m_notes }
              };
    }
  }

  /// <summary>
  /// Get Rhino Object Types
  /// </summary>
  public static class RhinoObject
  {
    /// <summary>
    /// Gets Rhino document objects (file3dmobjects, dimension styles, hatch patterns, layers, linetypes, materials, views)
    /// </summary>
    /// <param name="rh_File3dm">The Rhino model object</param>
    /// <returns name="attributes">document attributes</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    [MultiReturn(new[] { "File3dmObjects", "HatchPatterns", "Layers", "Linetypes", "Materials", "Views" })]
    public static Dictionary<string, object> Get_DocumentObjects(File3dm rh_File3dm)
    {
      List<Rhino.DocObjects.HatchPattern> m_hatchpatterns = rh_File3dm.HatchPatterns.ToList();
      List<Rhino.DocObjects.Layer> m_layers = rh_File3dm.Layers.ToList();
      List<Rhino.DocObjects.Linetype> m_linetypes = rh_File3dm.Linetypes.ToList();
      List<Rhino.DocObjects.Material> m_materials = rh_File3dm.Materials.ToList();
      List<Rhino.DocObjects.ViewInfo> m_views = rh_File3dm.Views.ToList();

      // objects
      List<File3dmObject> m_objslist = new List<File3dmObject>();
      List<string> m_names = new List<string>();

      // search through each layer
      foreach (Rhino.DocObjects.Layer lay in rh_File3dm.Layers)
      {
        File3dmObject[] m_objs = rh_File3dm.Objects.FindByLayer(lay.Name);
        foreach (File3dmObject obj in m_objs)
        {
          m_objslist.Add(obj);
        }
      }

      // return info
      return new Dictionary<string, object>
              {
                  { "File3dmObjects", m_objslist },
                  { "HatchPatterns", m_hatchpatterns },
                  { "Layers", m_layers },
                  { "Linetypes", m_linetypes },
                  { "Materials", m_materials },
                  { "Views", m_views }
              };
    }

    /// <summary>
    /// Gets Rhino all Rhino 3dm Objects
    /// </summary>
    /// <param name="rh_model">The Rhino model object (File3dm)</param>
    /// <returns name="objects">The list of Rhino objects.</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    public static File3dmObject[] Get_RhinoObjects(File3dm rh_model)
    {
      List<File3dmObject> m_objslist = new List<File3dmObject>();
      List<string> m_names = new List<string>();

      // search through each layer
      foreach (Rhino.DocObjects.Layer lay in rh_model.Layers)
      {
        File3dmObject[] m_objs = rh_model.Objects.FindByLayer(lay.Name);
        foreach (File3dmObject obj in m_objs)
        {
          m_objslist.Add(obj);
        }
      }

      // all objects
      return m_objslist.ToArray();
    }

    /// <summary>
    /// Gets Rhino File3dmObjects from a model using a layer name.
    /// </summary>
    /// <param name="rh_model">The Rhino model object (File3dm)</param>
    /// <param name="layer">The name of the layer</param>
    /// <returns name="objects">The list of Rhino objects.</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    public static File3dmObject[] Get_RhinoObjectsByLayer(File3dm rh_model, string layer)
    {
      File3dmObject[] objects = rh_model.Objects.FindByLayer(layer);
      return objects;
    }

    /// <summary>
    /// Gets Rhino layer names.
    /// </summary>
    /// <param name="rh_File3dm">The Rhino model object</param>
    /// <returns name="layers">List of Rhino layer names</returns>
    /// <search>case,rhino,model,layers,3dm,rhynamo</search>
    public static List<string> Get_RhinoLayerNames(File3dm rh_File3dm)
    {
      List<string> m_names = new List<string>();
      foreach (Rhino.DocObjects.Layer lay in rh_File3dm.Layers)
      {
        m_names.Add(lay.Name);
      }
      return m_names;
    }
  }

  /// <summary>
  /// Get Rhino Object Attribute info
  /// </summary>
  public static class RhinoObjectAttributes
  {
    /// <summary>
    /// Rhino Hatch Pattern Attributes
    /// </summary>
    /// <param name="rh_hatchpattern">Rhino Hatch Pattern</param>
    /// <search>case,rhino,model,3dm,rhynamo,hatch</search>
    [MultiReturn(new[] { "Name", "Index", "Description", "FillType" })]
    public static Dictionary<string, object> Get_HatchPatternAttributes(Rhino.DocObjects.HatchPattern rh_hatchpattern)
    {
      string m_name = rh_hatchpattern.Name;
      string m_index = rh_hatchpattern.Index.ToString();
      string m_description = rh_hatchpattern.Description;
      string m_filltype = rh_hatchpattern.FillType.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "Description", m_description },
                  { "FillType", m_filltype }
              };
    }

    /// <summary>
    /// Rhino Layer Attributes
    /// </summary>
    /// <param name="rh_layer">Rhino Layer</param>
    /// <search>case,rhino,model,3dm,rhynamo,layer</search>
    [MultiReturn(new[] { "Name", "Index", "LinetypeIndex", "MaterialIndex", "Color", "PlotColor", "GUID" })]
    public static Dictionary<string, object> Get_LayerAttributes(Rhino.DocObjects.Layer rh_layer)
    {
      string m_name = rh_layer.Name;
      int m_index = rh_layer.LayerIndex;
      int m_linetype = rh_layer.LinetypeIndex;
      int m_materialindex = rh_layer.RenderMaterialIndex;
      System.Drawing.Color m_color = rh_layer.Color;
      System.Drawing.Color m_plotcolor = rh_layer.PlotColor;
      string m_guid = rh_layer.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "LinetypeIndex", m_linetype },
                  { "MaterialIndex", m_materialindex},
                  { "Color", m_color },
                  { "PlotColor", m_plotcolor },
                  { "GUID", m_guid}
              };
    }

    /// <summary>
    /// Rhino Linetype Attributes
    /// </summary>
    /// <param name="rh_layer">Rhino Linetype</param>
    /// <search>case,rhino,model,3dm,rhynamo,linetype</search>
    [MultiReturn(new[] { "Name", "Index", "SegmentCount", "PatternLength", "GUID" })]
    public static Dictionary<string, object> Get_LinetypeAttributes(Rhino.DocObjects.Linetype rh_linetype)
    {
      string m_name = rh_linetype.Name;
      int m_index = rh_linetype.LinetypeIndex;
      int m_segmentcount = rh_linetype.SegmentCount;
      double m_patternlength = rh_linetype.PatternLength;
      string m_guid = rh_linetype.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "SegmentCount", m_segmentcount },
                  { "PatternLength", m_patternlength},
                  { "GUID", m_guid }
              };
    }

    /// <summary>
    /// Rhino Material Attributes
    /// </summary>
    /// <param name="rh_layer">Rhino Material</param>
    /// <search>case,rhino,model,3dm,rhynamo,material</search>
    [MultiReturn(new[] { "Name", "Index", "IndexOfRefraction", "Reflectivity", "Transparency", "AmbientColor","DiffuseColor","ReflectionColor","RefractionColor","TransparentColor","GUID" })]
    public static Dictionary<string, object> Get_MaterialAttributes(Rhino.DocObjects.Material rh_material)
    {
      string m_name = rh_material.Name;
      int m_index = rh_material.MaterialIndex;
      double m_intexrefract = rh_material.IndexOfRefraction;
      double m_reflectivity = rh_material.Reflectivity;
      double m_transparency = rh_material.Transparency;
      System.Drawing.Color m_ambientcol = rh_material.AmbientColor;
      System.Drawing.Color m_diffusecol = rh_material.DiffuseColor;
      System.Drawing.Color m_reflectioncol = rh_material.ReflectionColor;
      System.Drawing.Color m_refractioncol = rh_material.ReflectionColor;
      System.Drawing.Color m_transparentcol = rh_material.TransparentColor;
      string m_guid = rh_material.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "IndexOfRefraction", m_intexrefract },
                  { "Reflectivty", m_reflectivity },
                  { "Transparency", m_transparency },
                  { "AmbientColor", m_ambientcol },
                  { "DiffuseColor", m_diffusecol },
                  { "ReflectionColor", m_reflectioncol },
                  { "RefractionColor", m_refractioncol },
                  { "TransparentColor", m_transparentcol },
                  { "GUID", m_guid }
              };
    }

    /// <summary>
    /// Rhino View Attributes
    /// </summary>
    /// <param name="rh_layer">Rhino Linetype</param>
    /// <search>case,rhino,model,3dm,rhynamo,view</search>
    [MultiReturn(new[] { "Name", "Index", "Location", "Target", "LensLength", "GUID" })]
    public static Dictionary<string, object> Get_ViewAttributes(Rhino.DocObjects.ViewInfo rh_view)
    {
      string m_name = rh_view.Name;
      Autodesk.DesignScript.Geometry.Point m_location = Autodesk.DesignScript.Geometry.Point.ByCoordinates(rh_view.Viewport.CameraLocation.X, rh_view.Viewport.CameraLocation.Y, rh_view.Viewport.CameraLocation.Z);
      Autodesk.DesignScript.Geometry.Point m_target = Autodesk.DesignScript.Geometry.Point.ByCoordinates(rh_view.Viewport.TargetPoint.X, rh_view.Viewport.TargetPoint.Y, rh_view.Viewport.TargetPoint.Z);
      double m_lens = rh_view.Viewport.Camera35mmLensLength;
      string m_guid = rh_view.Viewport.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Location", m_location },
                  { "Target", m_target },
                  { "LensLength", m_lens },
                  { "GUID", m_guid },
              };
    }

    /// <summary>
    /// Gets Rhino object names
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects.</param>
    /// <returns name = "names">List of Rhino object names.</returns>
    /// <search>case,rhino,model,names,3dm,rhynamo</search>
    [MultiReturn(new[] { "Name", "LayerIndex", "LinetypeIndex", "MaterialIndex", "Color", "PlotColor", "ObjectType", "GUID" })]
    public static Dictionary<string, object> Get_RhinoGeometryAttributes(File3dmObject rh_object)
    {
      string m_name = rh_object.Name;
      int m_layerindex = rh_object.Attributes.LayerIndex;
      int m_linetypeindex = rh_object.Attributes.LinetypeIndex;
      int m_materialindex = rh_object.Attributes.MaterialIndex;
      System.Drawing.Color m_objcolor = rh_object.Attributes.ObjectColor;
      System.Drawing.Color m_objplotcolor = rh_object.Attributes.PlotColor;
      string m_guid = rh_object.Attributes.ObjectId.ToString();
      string m_type = rh_object.Geometry.ObjectType.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "LayerIndex", m_layerindex },
                  { "LinetypeIndex", m_linetypeindex },
                  { "MaterialIndex", m_materialindex },
                  { "Color", m_objcolor },
                  { "PlotColor", m_objplotcolor },
                  { "ObjectType", m_type },
                  { "GUID", m_guid },
              };
    }
  }

  /// <summary>
  /// Convert Rhino Geometry
  /// </summary>
  public static class RhinoTranslationToDS
  {

    /// <summary>
    /// Converts Rhino objects to DesignScript geometry
    /// </summary>
    /// <param name="rh_objects">List of Rhino Objects</param>
    /// <returns name="geometry">DesignScript Geometry</returns>
    /// <search>case,rhino,model,3dm,geometry,convert,rhynamo</search>
    [MultiReturn(new[] { "Points", "Curves", "Breps", "Extrusions", "Meshes" })]
    public static Dictionary<string, object> Rhino_AllGeometryToDS(File3dmObject[] rh_objects)
    {
      // rhino curve list
      List<Rhino.Geometry.Point> rh_points = new List<Rhino.Geometry.Point>();
      List<Rhino.Geometry.Curve> rh_curves = new List<Rhino.Geometry.Curve>();
      List<Rhino.Geometry.Brep> rh_breps = new List<Rhino.Geometry.Brep>();
      List<Rhino.Geometry.Extrusion> rh_extrusions = new List<Rhino.Geometry.Extrusion>();
      List<Rhino.Geometry.Mesh> rh_meshes = new List<Rhino.Geometry.Mesh>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the geometry is a Point
        if (geo is Rhino.Geometry.Point)
        {
          // add point to Rhino points list
          Rhino.Geometry.Point pt = geo as Rhino.Geometry.Point;
          rh_points.Add(pt);
        }

        // check if geometry is a curve
        if (geo is Rhino.Geometry.Curve)
        {
          // add curve to list
          Rhino.Geometry.Curve ln = geo as Rhino.Geometry.Curve;
          rh_curves.Add(ln);
        }

        // check if geometry is a brep
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          rh_breps.Add(rh_brep);
        }

        // get breps
        if (geo is Rhino.Geometry.Extrusion)
        {
          Rhino.Geometry.Extrusion rh_ext = geo as Rhino.Geometry.Extrusion;

          rh_extrusions.Add(rh_ext);
        }

        // check if geometry is a mesh
        if (geo is Rhino.Geometry.Mesh)
        {
          Rhino.Geometry.Mesh rh_mesh = geo as Rhino.Geometry.Mesh;
          rh_meshes.Add(rh_mesh);
        }
      }

      // conversion to design script
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();

      List<Autodesk.DesignScript.Geometry.Point> ds_points = new List<Autodesk.DesignScript.Geometry.Point>();
      List<Autodesk.DesignScript.Geometry.Curve> ds_curves = rh_ds.Convert_CurvesToDS(rh_curves);
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_breps = rh_ds.Convert_BrepsToDS(rh_breps);
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_extrusions = rh_ds.Convert_ExtrusionToDS(rh_extrusions);
      List<Autodesk.DesignScript.Geometry.Mesh> ds_meshes = rh_ds.Convert_MeshToDS(rh_meshes);

      return new Dictionary<string, object>
              {
                  { "points", ds_points },
                  { "curves", ds_curves },
                  { "breps", ds_breps },
                  { "extrusions", ds_extrusions },
                  { "meshes", ds_meshes }
              };
    }

    /// <summary>
    /// Converts Rhino arc objects to DesignScript arcs.
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through</param>
    /// <returns name="arcs">DesignScript Arcs</returns>
    /// <search>case,rhino,model,3dm,arc,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_ArcsToDS(File3dmObject[] rh_objects)
    {
      //List of Rhino lines
      List<Rhino.Geometry.ArcCurve> rh_arcs = new List<Rhino.Geometry.ArcCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is an arc curve
        if (geo is Rhino.Geometry.ArcCurve)
        {
          // add arc curve to arc list
          Rhino.Geometry.ArcCurve arc = geo as Rhino.Geometry.ArcCurve;
          rh_arcs.Add(arc);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino arcs to DesignScript arcs
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Arc> ds_arcs = rh_ds.Convert_ArcsToDS(rh_arcs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_arcs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Breps to DesignScript PolySurface objects
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name = "ds_polysurface">A list of DesignScript polysurfaces</returns>
    /// <search>case,rhino,model,3dm,nurbs,brep,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_BrepsToDS(File3dmObject[] rh_objects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.Brep> rh_breps = new List<Rhino.Geometry.Brep>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          rh_breps.Add(rh_brep);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_polysurfaces = rh_ds.Convert_BrepsToDS(rh_breps);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polysurfaces },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino circle objects to DesignScript circles
    /// </summary>
    /// <param name="rh_object">Rhino circle objects</param>
    /// <returns name = "ds_circle"></returns>
    /// <search>case,rhino,model,3dm,nurbs,circle,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_CirclesToDS(List<File3dmObject> rh_object)
    {
      List<Rhino.Geometry.Curve> rh_circles = new List<Rhino.Geometry.Curve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_object)
      {
        GeometryBase geo = obj.Geometry;
        if (geo is Rhino.Geometry.Curve)
        {
          Rhino.Geometry.Curve crv = (Rhino.Geometry.Curve)geo;

          // determine if curve is a circle
          if (crv.IsCircle() == true)
          {
            rh_circles.Add(crv);

            // attributes
            m_names.Add(obj.Attributes.Name);
            m_layerindeces.Add(obj.Attributes.LayerIndex);
            m_colors.Add(obj.Attributes.ObjectColor);
            m_guids.Add(obj.Attributes.ObjectId.ToString());
          }
        }
      }
      // create designscript circles
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Circle> ds_circles = rh_ds.Convert_CirclesToDS(rh_circles);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_circles },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino curve objects to DesignScript curves (lines, arcs, nurbs, polyline, polycurve)
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name="curves">List of DesignScript curve objects</returns>
    /// <search>case,rhino,model,curve,3dm,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static new Dictionary<string, object> Rhino_CurvesToDS(File3dmObject[] rh_objects)
    {
      // rhino curve list
      List<Rhino.Geometry.Curve> rh_curves = new List<Rhino.Geometry.Curve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is a curve
        if (geo is Rhino.Geometry.Curve)
        {
          // add curve to list
          Rhino.Geometry.Curve ln = geo as Rhino.Geometry.Curve;
          rh_curves.Add(ln);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // conversion to design script
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Curve> ds_curves = rh_ds.Convert_CurvesToDS(rh_curves);

      // return curve objects
      return new Dictionary<string, object>
              {
                  { "Geometry", ds_curves },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Extrusions to DesignScript PolySurface objects
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name = "ds_polysurface">A list of DesignScript polysurfaces</returns>
    /// <search>case,rhino,model,3dm,nurbs,extrusion,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_ExtrusionToDS(File3dmObject[] rh_objects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.Extrusion> rh_extrusion = new List<Rhino.Geometry.Extrusion>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Extrusion)
        {
          Rhino.Geometry.Extrusion rh_brep = geo as Rhino.Geometry.Extrusion;

          rh_extrusion.Add(rh_brep);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_polysurfaces = rh_ds.Convert_ExtrusionToDS(rh_extrusion);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polysurfaces },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino line objects to DesignScript lines.
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through</param>
    /// <returns name="lines">DesignScript Lines</returns>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_LinesToDS(File3dmObject[] rh_objects)
    {
      //List of Rhino lines
      List<Rhino.Geometry.LineCurve> rh_lines = new List<Rhino.Geometry.LineCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is a line curve
        if (geo is Rhino.Geometry.LineCurve)
        {
          // add line curve to lines list
          Rhino.Geometry.LineCurve ln = geo as Rhino.Geometry.LineCurve;
          rh_lines.Add(ln);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino lines to DesignScript lines
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Line> ds_lines = rh_ds.Convert_LinesToDS(rh_lines);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_lines },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Convert Rhino meshes to DesignScript meshes
    /// </summary>
    /// <param name="rh_objects">Rhino mesh objects</param>
    /// <returns name = "meshes">DesignScript Mesh</returns>
    /// <search>case,rhino,model,3dm,nurbs,mesh,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_MeshToDS(File3dmObject[] rh_objects)
    {
      List<Rhino.Geometry.Mesh> rh_meshes = new List<Rhino.Geometry.Mesh>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;
        if (geo is Rhino.Geometry.Mesh)
        {
          Rhino.Geometry.Mesh rh_mesh = geo as Rhino.Geometry.Mesh;
          rh_meshes.Add(rh_mesh);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Mesh> ds_meshes = rh_ds.Convert_MeshToDS(rh_meshes);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_meshes },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino NurbsCurve objects to DesignScript NurbsCurves
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name = "nurbs">DesignScript NurbsCurves</returns>
    /// <search>case,rhino,model,3dm,nurbs,curve,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_NurbsCurvesToDS(File3dmObject[] rh_objects)
    {
      // list of Rhino Nurbs curves
      List<Rhino.Geometry.NurbsCurve> rh_nurbs = new List<Rhino.Geometry.NurbsCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a nurbs curve
        if (geo is Rhino.Geometry.NurbsCurve)
        {
          // add nurbs curve to nurbs list
          Rhino.Geometry.NurbsCurve nurbs = geo as Rhino.Geometry.NurbsCurve;
          rh_nurbs.Add(nurbs);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.NurbsCurve> ds_nurbs = rh_ds.Convert_NurbsCurveToDS(rh_nurbs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_nurbs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Untrimmed Rhino NurbsSurface objects to DesignScript NurbsSurfaces
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through</param>
    /// <returns name ="nurbs">DesignScript NurbsSurfaces</returns>
    /// <search>case,rhino,model,3dm,nurbs,surface,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_NurbsSurfaceToDS(File3dmObject[] rh_objects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.NurbsSurface> rh_nurbs = new List<Rhino.Geometry.NurbsSurface>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          // get breps that are surfaces
          if (rh_brep.IsSurface == true)
          {
            // get nurbs surface
            Rhino.Geometry.NurbsSurface rh_srf = rh_brep.Faces[0].ToNurbsSurface();
            rh_nurbs.Add(rh_srf);

            // attributes
            m_names.Add(obj.Attributes.Name);
            m_layerindeces.Add(obj.Attributes.LayerIndex);
            m_colors.Add(obj.Attributes.ObjectColor);
            m_guids.Add(obj.Attributes.ObjectId.ToString());
          }
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.NurbsSurface> ds_nurbs = rh_ds.Convert_NurbsSurfaceToDS(rh_nurbs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_nurbs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Polyline objects to DesignScript Polyline
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name = "nurbs">DesignScript PolyCurves</returns>
    /// <search>case,rhino,model,3dm,polyline,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PolylinesToDS(File3dmObject[] rh_objects)
    {
      // list of Rhino Poly curves
      List<Rhino.Geometry.PolylineCurve> rh_polycrv = new List<Rhino.Geometry.PolylineCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a Poly curve
        if (geo is Rhino.Geometry.PolylineCurve)
        {
          // add nurbs curve to Poly list
          Rhino.Geometry.PolylineCurve polycrv = geo as Rhino.Geometry.PolylineCurve;
          rh_polycrv.Add(polycrv);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolyCurve> ds_polycrvs = rh_ds.Convert_PolylineToDS(rh_polycrv);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polycrvs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino PolyCurve objects to DesignScript PolyCurves
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name = "nurbs">DesignScript PolyCurves</returns>
    /// <search>case,rhino,model,3dm,polycurve,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PolyCurvesToDS(File3dmObject[] rh_objects)
    {
      // list of Rhino Poly curves
      List<Rhino.Geometry.PolyCurve> rh_polycrv = new List<Rhino.Geometry.PolyCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a Poly curve
        if (geo is Rhino.Geometry.PolyCurve)
        {
          // add nurbs curve to Poly list
          Rhino.Geometry.PolyCurve polycrv = geo as Rhino.Geometry.PolyCurve;
          rh_polycrv.Add(polycrv);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolyCurve> ds_polycrvs = rh_ds.Convert_PolyCurveToDS(rh_polycrv);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polycrvs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino point objects to DesignScript points.
    /// </summary>
    /// <param name="rh_objects">List of Rhino objects to search through.</param>
    /// <returns name="points">DesignScript Points</returns>
    /// <search>case,rhino,model,3dm,point,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PointsToDS(File3dmObject[] rh_objects)
    {
      // list of Rhino points
      List<Rhino.Geometry.Point> rh_points = new List<Rhino.Geometry.Point>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in rh_objects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the geometry is a Point
        if (geo is Rhino.Geometry.Point)
        {
          // add point to Rhino points list
          Rhino.Geometry.Point pt = geo as Rhino.Geometry.Point;
          rh_points.Add(pt);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }
      // convert Rhino points to DesignScript points
      CASE.classes.clsGeometryConversionUtils rh_ds = new CASE.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Point> ds_points = rh_ds.Convert_PointsToDS(rh_points);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_points },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

  }
}
