﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Rhino.FileIO;
using Rhino.Geometry;

namespace Interoperability.WriteRhino
{
  /// <summary>
  /// Translate Dynamo Geometry to Rhino Geometry
  /// </summary>
  public static class CreateRhinoGeometry
  {
    /// <summary>
    /// Converts DesignSript PolySurfaces to Rhino Breps
    /// </summary>
    /// <param name="ds_polysurface">DesignScript Breps</param>
    /// <returns name="Brep">Rhino Brep</returns>
    /// <search>case,rhino,3dn,rhynamo,brep</search>
    public static Rhino.Geometry.Brep DS_BrepToRhino(Autodesk.DesignScript.Geometry.PolySurface ds_polysurface)
    {
      try
      {
        CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Brep rh_brp = ds_rh.Convert_PolySurfaceTo3dm(ds_polysurface);
        return rh_brp;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts DesignScript curves to Rhino curve objects
    /// </summary>
    /// <param name="ds_curve">DesignScript Curves</param>
    /// <returns name="rh_curve">Rhino curves</returns>
    /// <search>case,rhino,3dm,rhynamo,curve</search>
    public static Rhino.Geometry.Curve DS_CurvesToRhino(Autodesk.DesignScript.Geometry.Curve ds_curve)
    {
      try
      {
        CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Curve rh_crv = ds_rh.Convert_CurvesTo3dm(ds_curve);
        return rh_crv;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts DesignScript meshes to Rhino meshes
    /// </summary>
    /// <param name="ds_mesh">DesignScript meshes</param>
    /// <returns name="mesh">Rhino meshes</returns>
    /// <search>case,rhino,3dm,rhynamo,mesh</search>
    public static Rhino.Geometry.Mesh DS_MeshToRhino(Autodesk.DesignScript.Geometry.Mesh ds_mesh)
    {
      try
      {
        CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Mesh rh_mesh = ds_rh.Convert_MeshTo3dm(ds_mesh);
        return rh_mesh;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts DesignScript nurbs surfaces to Rhino nurbs surface objects
    /// </summary>
    /// <param name="ds_NurbsSurface">DesignScript Nurbs Surfaces</param>
    /// <returns name="NurbsSurface">Rhino Nurbs surfaces</returns>
    /// <search>case,rhino,3dm,rhynamo,nurbssurface</search>
    public static Rhino.Geometry.NurbsSurface DS_NurbsSurfaceToRhino(Object ds_surface)
    {
      try
      {
        Rhino.Geometry.NurbsSurface rh_srfs = null;
        if (ds_surface is Autodesk.DesignScript.Geometry.NurbsSurface)
        {
          CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
          Autodesk.DesignScript.Geometry.NurbsSurface ds_nurbssurface = (Autodesk.DesignScript.Geometry.NurbsSurface)ds_surface;
          rh_srfs = ds_rh.Convert_SurfacesTo3dm(ds_nurbssurface);
        }
        else if (ds_surface is Autodesk.DesignScript.Geometry.Surface)
        {
          CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
          Autodesk.DesignScript.Geometry.Surface ds_srf = (Autodesk.DesignScript.Geometry.Surface)ds_surface;
          Autodesk.DesignScript.Geometry.NurbsSurface ds_nurbssurface = ds_srf.ToNurbsSurface();
          rh_srfs = ds_rh.Convert_SurfacesTo3dm(ds_nurbssurface);
        }
        return rh_srfs;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts DesignScript points to Rhino point objects
    /// </summary>
    /// <param name="ds_points">DesignScript Points</param>
    /// <returns name="Point3d">Rhino points</returns>
    /// <search>case,rhino,3dm,rhynamo,point</search>
    public static Rhino.Geometry.Point DS_PointsToRhino(Autodesk.DesignScript.Geometry.Point ds_point)
    {
      try
      {
        CASE.classes.clsGeometryConversionUtils ds_rh = new CASE.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Point rh_pt = ds_rh.Convert_PointsTo3dm(ds_point);

        return rh_pt;
      }
      catch { return null; }
    }
  }

  /// <summary>
  /// Create Rhino Objects with Attributes
  /// </summary>
  public static class CreateRhinoObjects
  {
    /// <summary>
    /// Creates a true System color object for Rhino layers and object colors.
    /// </summary>
    /// <param name="Red">Red (0-255)</param>
    /// <param name="Green">Green (0-255)</param>
    /// <param name="Blue">Blue (0-255)</param>
    /// <returns>System.Drawing.Color</returns>
    /// <search>case,rhino,3dm,rhynamo,layer,color,model</search>
    public static System.Drawing.Color Create_RhinoColor(int Red, int Green, int Blue)
    {
      try
      {
        return System.Drawing.Color.FromArgb(Red, Green, Blue);
      }
      catch { return System.Drawing.Color.FromArgb(0, 0, 0); }
    }

    /// <summary>
    /// Create a Rhino Layer
    /// </summary>
    /// <param name="name">Layer name</param>
    /// <param name="color">Layer color</param>
    /// <returns>Layer object</returns>
    /// <search>case,rhino,3dm,rhynamo,layer,model</search>
    public static Rhino.DocObjects.Layer Create_RhinoLayer(string layerName, System.Drawing.Color layerColor)
    {
      try
      {
        Rhino.DocObjects.Layer m_layer = new Rhino.DocObjects.Layer();
        m_layer.Name = layerName;
        m_layer.Color = layerColor;
        return m_layer;
      }
      catch { return null; }
    }

    /// <summary>
    /// Create Rhino Object
    /// </summary>
    /// <param name="rh_geometry">Geometry</param>
    /// <param name="rh_name">Object Name</param>
    /// <param name="rh_objcolor">Object Color</param>
    /// <param name="rh_layer">Layer</param>
    /// <returns>Rhino Object</returns>
    /// <search>case,rhino,3dm,rhynamo,object,model</search>
    public static Object Create_RhinoObject(Object rh_geometry, string rh_name, System.Drawing.Color rh_objcolor, Rhino.DocObjects.Layer rh_layer)
    {
      try
      {
        CASE.classes.clsRhinoObject m_rhinoobj = new CASE.classes.clsRhinoObject(rh_geometry, rh_layer, rh_name, rh_objcolor);
        return (Object)m_rhinoobj;
      }
      catch { return null; }
    } 
  }

  /// <summary>
  /// Save the Rhino model to a File
  /// </summary>
  public static class SaveRhino3dmModel
  {
    /// <summary>
    /// Save Rhino Model
    /// </summary>
    /// <param name="FilePath">Location of the Rhino File</param>
    /// <param name="SaveBool">Toggle to Save the File</param>
    /// <param name="RhinoObjects">Rhino Geometry to Save</param>
    /// <returns>Rhino 3dm Model Object</returns>
    /// <search>case,rhino,3dm,rhynamo,save,model</search>
    public static File3dm Save_Rhino3dmModel(string FilePath, bool SaveBool, Object[] RhinoObjects)
    {
      try
      {
        // Rhino model
        File3dm m_model = new File3dm();

        // Create Default Layer
        Rhino.DocObjects.Layer m_defaultlayer = new Rhino.DocObjects.Layer();
        m_defaultlayer.Name = "Default";
        m_defaultlayer.LayerIndex = 0;
        m_defaultlayer.IsVisible = true;
        m_defaultlayer.IsLocked = false;
        m_defaultlayer.Color = System.Drawing.Color.FromArgb(0, 0, 0);

        // Add default layer to model
        m_model.Layers.Add(m_defaultlayer);
        m_model.Polish();

        //iterate through objects
        for (int i = 0; i < RhinoObjects.Length; i++)
        {
          // Rhino object
          CASE.classes.clsRhinoObject m_rhinoobj = RhinoObjects[i] as CASE.classes.clsRhinoObject;

          // rhino object information
          Object m_objgeo = m_rhinoobj.RhinoObject;
          string m_objname = m_rhinoobj.ObjectName;
          System.Drawing.Color m_objcolor = m_rhinoobj.ObjectColor;
          Rhino.DocObjects.Layer m_objlayer = m_rhinoobj.ObjectLayer;
          m_objlayer.LayerIndex = m_model.Layers.Count;

          // object attributes
          Rhino.DocObjects.ObjectAttributes objatt = new Rhino.DocObjects.ObjectAttributes();
          objatt.Name = m_objname;
          objatt.ObjectColor = m_objcolor;

          if (m_objcolor != m_objlayer.Color)
          {
            objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromObject;
          }
          else
          {
            objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromLayer;
          }

          // setup layer
          try
          {
            bool m_layerexists = false;
            int m_layerindex = -1;
            for (int j = 0; j < m_model.Layers.Count; j++)
            {
              Rhino.DocObjects.Layer l = m_model.Layers[j];
              if (l.Name == m_objlayer.Name)
              {
                m_layerexists = true;
                m_layerindex = j;
              }
            }

            // if the layer exists
            if (m_layerexists == true)
            {
              // set the object attribute layer index
              objatt.LayerIndex = m_layerindex;
            }
            else
            {
              // add layer to the model and set the object attribute
              m_model.Layers.Add(m_objlayer);
              m_model.Polish();
              objatt.LayerIndex = m_objlayer.LayerIndex;
            }
          }
          catch { }

          // check if point
          if (m_objgeo is Rhino.Geometry.Point)
          {
            Rhino.Geometry.Point pt = (Rhino.Geometry.Point)m_objgeo;
            double x = pt.Location.X;
            double y = pt.Location.Y;
            double z = pt.Location.Z;

            Rhino.Geometry.Point3d pt3d = new Point3d(x, y, z);
            m_model.Objects.AddPoint(pt3d, objatt);
          }

          // check if curve
          if (m_objgeo is Rhino.Geometry.Curve)
          {
            Rhino.Geometry.Curve cv = m_objgeo as Rhino.Geometry.Curve;
            m_model.Objects.AddCurve(cv, objatt);
          }

          //check if surface
          if (m_objgeo is Rhino.Geometry.NurbsSurface)
          {
            Rhino.Geometry.Surface srf = m_objgeo as Rhino.Geometry.Surface;
            m_model.Objects.AddSurface(srf, objatt);
          }

          //check if brep
          if (m_objgeo is Rhino.Geometry.Brep)
          {
            Rhino.Geometry.Brep brp = m_objgeo as Rhino.Geometry.Brep;
            m_model.Objects.AddBrep(brp, objatt);
          }

          //check if mesh
          if (m_objgeo is Rhino.Geometry.Mesh)
          {
            Rhino.Geometry.Mesh msh = m_objgeo as Rhino.Geometry.Mesh;
            m_model.Objects.AddMesh(msh, objatt);
          }
        }

        // write file
        if (SaveBool == true)
        {
          try
          {
            // write model file
            m_model.Write(FilePath, 1);
          }
          catch {  }
        }

        return m_model;
      }
      catch { return null; }
    }

    /// <summary>
    /// Creates a Rhino model
    /// </summary>
    /// <param name="rh_objs">Rhino Objects</param>
    /// <returns>Rhino Model Object</returns>
    /// <search>case,rhino,3dm,rhynamo,model</search>
    //////public static File3dm Create_RhinoModel(Object[] rh_objects)
    //////{
    //////  try
    //////  {
    //////    // Rhino model
    //////    File3dm m_model = new File3dm();

    //////    // Create Default Layer
    //////    Rhino.DocObjects.Layer m_defaultlayer = new Rhino.DocObjects.Layer();
    //////    m_defaultlayer.Name = "Default";
    //////    m_defaultlayer.LayerIndex = 0;
    //////    m_defaultlayer.IsVisible = true;
    //////    m_defaultlayer.IsLocked = false;
    //////    m_defaultlayer.Color = System.Drawing.Color.FromArgb(0, 0, 0);

    //////    // Add default layer to model
    //////    m_model.Layers.Add(m_defaultlayer);
    //////    m_model.Polish();

    //////    //iterate through objects
    //////    for (int i = 0; i < rh_objects.Length; i++)
    //////    {
    //////      // Rhino object
    //////      CASE.classes.clsRhinoObject m_rhinoobj = rh_objects[i] as CASE.classes.clsRhinoObject;

    //////      // rhino object information
    //////      Object m_objgeo = m_rhinoobj.RhinoObject;
    //////      string m_objname = m_rhinoobj.ObjectName;
    //////      System.Drawing.Color m_objcolor = m_rhinoobj.ObjectColor;
    //////      Rhino.DocObjects.Layer m_objlayer = m_rhinoobj.ObjectLayer;
    //////      m_objlayer.LayerIndex = m_model.Layers.Count;

    //////      // object attributes
    //////      Rhino.DocObjects.ObjectAttributes objatt = new Rhino.DocObjects.ObjectAttributes();
    //////      objatt.Name = m_objname;
    //////      objatt.ObjectColor = m_objcolor;

    //////      if (m_objcolor != m_objlayer.Color)
    //////      {
    //////        objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromObject;
    //////      }
    //////      else
    //////      {
    //////        objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromLayer;
    //////      }

    //////      // setup layer
    //////      try
    //////      {
    //////        bool m_layerexists = false;
    //////        int m_layerindex = -1;
    //////        for (int j = 0; j < m_model.Layers.Count; j++)
    //////        {
    //////          Rhino.DocObjects.Layer l = m_model.Layers[j];
    //////          if (l.Name == m_objlayer.Name)
    //////          {
    //////            m_layerexists = true;
    //////            m_layerindex = j;
    //////          }
    //////        }

    //////        // if the layer exists
    //////        if (m_layerexists == true)
    //////        {
    //////          // set the object attribute layer index
    //////          objatt.LayerIndex = m_layerindex;
    //////        }
    //////        else
    //////        {
    //////          // add layer to the model and set the object attribute
    //////          m_model.Layers.Add(m_objlayer);
    //////          m_model.Polish();
    //////          objatt.LayerIndex = m_objlayer.LayerIndex;
    //////        }
    //////      }
    //////      catch { }

    //////      // check if point
    //////      if (m_objgeo is Rhino.Geometry.Point)
    //////      {
    //////        Rhino.Geometry.Point pt = (Rhino.Geometry.Point)m_objgeo;
    //////        double x = pt.Location.X;
    //////        double y = pt.Location.Y;
    //////        double z = pt.Location.Z;

    //////        Rhino.Geometry.Point3d pt3d = new Point3d(x, y, z);
    //////        m_model.Objects.AddPoint(pt3d, objatt);
    //////      }

    //////      // check if curve
    //////      if (m_objgeo is Rhino.Geometry.Curve)
    //////      {
    //////        Rhino.Geometry.Curve cv = m_objgeo as Rhino.Geometry.Curve;
    //////        m_model.Objects.AddCurve(cv, objatt);
    //////      }

    //////      //check if surface
    //////      if (m_objgeo is Rhino.Geometry.NurbsSurface)
    //////      {
    //////        Rhino.Geometry.Surface srf = m_objgeo as Rhino.Geometry.Surface;
    //////        m_model.Objects.AddSurface(srf, objatt);
    //////      }

    //////      //check if brep
    //////      if (m_objgeo is Rhino.Geometry.Brep)
    //////      {
    //////        Rhino.Geometry.Brep brp = m_objgeo as Rhino.Geometry.Brep;
    //////        m_model.Objects.AddBrep(brp, objatt);
    //////      }

    //////      //check if mesh
    //////      if (m_objgeo is Rhino.Geometry.Mesh)
    //////      {
    //////        Rhino.Geometry.Mesh msh = m_objgeo as Rhino.Geometry.Mesh;
    //////        m_model.Objects.AddMesh(msh, objatt);
    //////      }
    //////    }
    //////    return m_model;
    //////  }
    //////  catch { return null; }

    //////}

    /// <summary>
    /// Save a Rhino Model file to a location
    /// </summary>
    /// <param name="filepath">File path for the 3D model file</param>
    /// <param name="rh_model">Rhino model</param>
    /// <returns name="message">Message, created or failed</returns>
    /// <search>case,rhino,3dm,rhynamo,save</search>
    //////public static string Save_Rhino3dmFile(string filepath, File3dm rh_model)
    //////{
    //////  try
    //////  {
    //////    // write model file
    //////    rh_model.Write(filepath, 1);
    //////    return "Rhino file saved";
    //////  }
    //////  catch (Exception x) { return x.ToString(); }
    //////}
  }
}
